import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AutoService } from './services/auto.service';
import { Auto } from './interfaces/auto';
import { UsuarioAutoService } from './services/usuario-auto.service';
import { UsuarioAuto } from './interfaces/usuario-auto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  auto: Auto[];
  list1: any[];
  list2: any[];
  editing: boolean = false;

  usuauto: UsuarioAuto = {
    user_id: null,
    car_id: null,
  }
  constructor(private formBuilder: FormBuilder, private autoService: AutoService, private usuarioAutoService: UsuarioAutoService) { }

  ngOnInit() {
    this.autoService.getListaUno().subscribe(data => { this.list1 = data });
    //console.log(this.list1);

    this.list1 = [
    ]

    this.list2 = [

    ];
    this.registerForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: ['', [Validators.email]],
      password: ['', [Validators.minLength(6)]],
      listauno: [this.list1],
      car_id: [this.list2],
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    /* this.submitted = true;
     stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(JSON.stringify(this.registerForm.value))*/



    if (this.editing) {
      this.usuarioAutoService.put(this.usuauto).subscribe((data) => {
        alert('UsuarioAuto actualizada');
        console.log(data);
      }, (error) => {
        alert('Ocurrio un error');
        console.log(error);
      });
    } else {
      this.usuarioAutoService.save(this.usuauto).subscribe((data) => {
        alert('UsuarioAuto guardada');
        console.log(data);
      }, (error) => {
        alert('Ocurrio un error');
        console.log(error);
      });
    }
  }
}

