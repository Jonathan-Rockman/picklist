import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Usuario } from '../interfaces/usuario';
import { Auto } from '../interfaces/auto';
import { UsuarioService } from '../services/usuario.service';
import { AutoService } from '../services/auto.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})

export class AppComponent implements OnInit {
  usuario: Usuario = {
    firstName: null,
    lastName: null,
    email: null,
    password: null,
    confirmPassword: null,
  };
  id: any;
  editing: boolean = false;
  usuarios: Usuario[];
  registerForm: FormGroup;
  submitted = false;
  // FORM BUILDER
  auto: Auto[];
  list1: any[];
  list2: any[];
  constructor(private formBuilder: FormBuilder, private autoService: AutoService,
    private usuarioService: UsuarioService, private activatedRoute: ActivatedRoute) {
    this.getAutos();
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.editing = true;
      this.usuarioService.get().subscribe((data: Usuario[]) => {
        this.usuarios = data;
        this.usuario = this.usuarios.find((m) => { return m.id == this.id });
        console.log(this.usuario);
      }, (error) => {
        console.log(error);
      });
    } else {
      this.editing = false;
    }
  }

  getAutos() {
    this.autoService.get().subscribe((data: Auto[]) => {
      this.auto = data;
    }, (error) => {
      console.log('Ocurrio un error');
    });
  }

  ngOnInit() {

    this.list1 = [
      { brand: 'Vw', year: 1995, color: 'Rojo' },
      { brand: 'Honda', year: 2001, color: 'Negro' },
      { brand: 'Jaguar', year: 2007, color: 'Morado' },
      { brand: 'Chevrolet', year: 2007, color: 'Blanco' },
      { brand: 'Acura', year: 2013, color: 'Amarillo' },
      { brand: 'Porsche', year: 2019, color: 'Gris' }
    ];
    this.list2 = [

    ];
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    if (this.editing) {
      this.usuarioService.put(this.usuario).subscribe((data) => {
        alert('Usuario actualizado');
        console.log(data);
      }, (error) => {
        alert('Ocurrio un error');
        console.log(error);
      });
    } else {
      this.usuarioService.save(this.usuario).subscribe((data) => {
        alert('Usuario guardada');
        console.log(data);
      }, (error) => {
        alert('Ocurrio un error');
        console.log(error);
      });
    }
    /*console.log(JSON.stringify(this.registerForm.value))*/
  }
}
