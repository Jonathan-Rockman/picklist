export interface Auto {
  id?: number;
  brand: string;
  year: number;
  color: string;
  created_at?: string;
  updated_at?: string;
}
