export interface Usuario {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
  created_at?: string;
  updated_at?: string;
}
