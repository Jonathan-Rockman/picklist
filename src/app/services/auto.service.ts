import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Auto } from '../interfaces/auto';

@Injectable({
  providedIn: 'root'
})
export class AutoService {
  API_ENDPOINT = 'http://127.0.0.1/pickbd/tables/public/api';
  constructor(private xxx: HttpClient) { }
  get() {
    return this.xxx.get(this.API_ENDPOINT + '/autos');
  }

  getListaUno() {
    let url = 'http://127.0.0.1/pickbd/tables/public/api/autos';
    return this.xxx.get<any[]>(url).pipe(map(res => res));
  }
  save(auto: Auto) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.xxx.post(this.API_ENDPOINT + '/movies', auto, { headers: headers });
  }

}
