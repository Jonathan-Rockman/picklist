import { TestBed } from '@angular/core/testing';

import { UsuarioAutoService } from './usuario-auto.service';

describe('UsuarioAutoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsuarioAutoService = TestBed.get(UsuarioAutoService);
    expect(service).toBeTruthy();
  });
});
