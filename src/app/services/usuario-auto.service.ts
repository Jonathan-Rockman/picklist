import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioAuto } from '../interfaces/usuario-auto';

@Injectable({
  providedIn: 'root'
})
export class UsuarioAutoService {
  API_ENDPOINT = 'http://127.0.0.1/pickbd/tables/public/api';
  constructor(private httpClient: HttpClient) { }
  get() {
    return this.httpClient.get(this.API_ENDPOINT + '/usuario_auto');
  }
  save(usuauto: UsuarioAuto) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.post(this.API_ENDPOINT + '/usuario_auto', usuauto, { headers: headers });
  }
  put(usauto) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpClient.put(this.API_ENDPOINT + '/usuario_auto/' + usauto.id, usauto, { headers: headers });
  }
  delete(id) {
    return this.httpClient.delete(this.API_ENDPOINT + '/usuario_auto/' + id);
  }
}
